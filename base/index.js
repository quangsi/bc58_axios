setTimeout(function () {
  console.log("Bật quảo cáo lần 2");
}, 2);
setTimeout(function () {
  console.log("Bật quảo cáo");
}, 0);
// đồng bộ

console.log(1);
console.log(2);
console.log(3);
// bất đồng bộ

// event loop https://www.google.com/search?q=event+loop&sca_esv=574803523&tbm=isch&sxsrf=AM9HkKkjhwZSwtHj-AQwjwOSZmYO0Li4vA:1697716664918&source=lnms&sa=X&sqi=2&ved=2ahUKEwjR-va1h4KCAxUDsFYBHazwAvYQ_AUoAXoECAUQAw&biw=1309&bih=711&dpr=2.2#imgrc=J577ofdGowWJfM&imgdii=5YLdymIa74W01M

// dùng axios để lấy data từ server

axios({
  url: "https://633ec05b0dbc3309f3bc5455.mockapi.io/product",
  method: "GET",
})
  .then(function (res) {
    console.log("😀 - res", res);
  })
  .catch(function (err) {
    console.log("😀 - err", err);
  });

//   promise : pendding (đợi), resolve (thành công), reject (thất bại)
