// render dssp
var idEdit = null;
// 1. gọi api lấy danh sách sp từ server
function fetchProductList() {
  turnOnLoading();
  axios({
    url: "https://633ec05b0dbc3309f3bc5455.mockapi.io/product",
    method: "GET",
  })
    .then(function (res) {
      renderProductList(res.data);
      turnOffLoading();
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("😀 - err", err);
    });
}

fetchProductList();
//   2. xoá 1 sp trên server

function deleteProduct(id) {
  turnOnLoading();
  axios({
    url: `https://633ec05b0dbc3309f3bc5455.mockapi.io/product/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      // gọi lại api lấy ds sp mới nhất từ server sau khi xoá thành công
      fetchProductList();
    })
    .catch(function (err) {
      turnOffLoading();
    });
}

function createProduct() {
  console.log("yess");

  var product = getDataForm();
  axios({
    url: "https://633ec05b0dbc3309f3bc5455.mockapi.io/product",
    method: "POST",
    data: product,
  })
    .then(function (res) {
      fetchProductList();
      // tắt modal sau khi thêm thành công
      $("#myModal").modal("hide");
    })
    .catch(function (err) {});
}

// sửa = lấy chi tiết + cập nhật

function editProduct(id) {
  // idEdit chứa id của sp được chọn khi user nhấn btn edit

  idEdit = id;
  $("#myModal").modal("show");
  // gọi api lấy chi tiết 1 món ăn
  axios({
    url: `https://633ec05b0dbc3309f3bc5455.mockapi.io/product/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res.data);
      var product = res.data;
      // hiển thị res lên form
      document.getElementById("TenSP").value = product.name;
      document.getElementById("GiaSP").value = product.price;
      document.getElementById("HinhSP").value = product.img;
      document.getElementById("MoTaSP").value = product.desc;
    })
    .catch(function (err) {
      console.log(err);
    });
}
function updateProduct() {
  var product = getDataForm();
  // gọi api cập nhật
  axios({
    url: `https://633ec05b0dbc3309f3bc5455.mockapi.io/product/${idEdit}`,
    method: "PUT",
    data: product,
  })
    .then(function (res) {
      // sau khi cập nhật thành công
      fetchProductList();
      $("#myModal").modal("hide");

      console.log(res);
    })
    .catch(function (err) {
      console.log(err);
    });
}

var color = ["red", "blue"];
color[1] = "white";
